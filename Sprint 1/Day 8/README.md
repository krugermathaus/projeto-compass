# Day 8

## NoSQL

#### Para o aprendizado de NoSQL será utilizado o MongoDB através do ambiente virtual HumongouS.io.

#### Diferentemente do SQL, no NoSQL não existem comandos como CREATE para criar um novo banco ou tabela. O tipo de dado utilizado também não são tabelas, mas sim "collections".

#### Para criar um novo banco, basta inserir um dado neste banco, através de um comando com uma sintaxe bem parecida com a orientação a objetos:

    db.nomeDaCollection.insertOne({ chave: "valor" })

Onde "db" se refere ao banco de dados, "nomeDaCollection é o nome da nova coleção, "insertOne" é o método para a inserção de dados e "chave: "valor"" é o dado a ser inserido, no formado document, semelhante ao JavaScript.

---

### Atividades realizadas:

### Após colar o Json de usuários, com base nos usuários listados, você deve:

#### 1. Realizar uma consulta que conte o número de registros existentes.

#### Comando:

    db.collection.count()

#### Retorno:

    [16];

#### 2. Realizar uma consulta para alterar o usuário com o nome "Teste Start" para "Teste Finish".

#### Comando:

    db.collection.updateOne({nome: 'Teste Start'},{$set:{nome: 'Teste Finish'}})


#### Retorno:

    [
     {
      acknowledged: true,
      modifiedCount: 1,
      upsertedId: null,
      upsertedCount: 0,
      matchedCount: 1,
     },
    ];

No entanto, como no caso do exercício de SQL, nada foi modificado na Collection.

#### 3. Realizar uma consulta para encontrar o usuário com o nome "Bruce Wayne".

#### Comando:

    db.collection.findOne({nome: 'Bruce Wayne'})

#### Retorno:

    [
     {
       _id: "6478cfd16c362a1798dbcfdb",
       nome: "Bruce Wayne",
       email: "brucewayne@gothan.com",
     },
    ];

#### 4. Realizar uma consulta para encontrar o usuário com o e-mail "ghost_silva@fantasma.com".

#### Comando:

    db.collection.findOne({email: 'ghost_silva@fantasma.com'})

#### Retorno:

    [
     {
      _id: "6478d05b32003888429188e2",
      nome: "Ghost Silva",
      email: "ghost_silva@fantasma.com",
     },
    ];

#### 5. Realizar uma consulta para deletar o usuário com e-mail "peterparker@marvel.com".

#### Comando:

    db.collection.remove({email: 'peterparker@marvel.com'})

#### Retorno: 

    .remove() is not yet supported

Novamente não houve retorno. Porém, desta vez aparentemente devido à plataforma Humongous não suportar o método "remove". É, parece que Peter se safou novamente.

![](https://preview.redd.it/jbqie4e8ctc81.png?width=1280&format=png&auto=webp&v=enabled&s=5172fd3dcae2e34c8a4cc1658468dff4676b6f41)

### Após colar o Json de produtos, com base nos produtos listados, você deve:

#### 1. Realizar uma consulta que apresente produtos com descrição vazia;

#### Comando:

    db.collection.find({descricao: ''})

#### Retorno:

    [
     {
      _id: "6478d2ee6c362a1798dbdcb7",
      nome: "caneca chopp",
      categoria: "utilitários",
      preco: 25.5,
      descricao: "",
     },
    {
      _id: "6478d2ee6c362a1798dbdcb8",
      nome: "copo grande térmico",
      categoria: "utilitários",
      preco: 35.9,
      descricao: "",
     },
    ];

#### 2. Realizar uma consulta que apresente produtos com a categoria "games";

#### Comando:

    db.collection.find({categoria: 'games'})

#### Retorno:

    [
    {
        _id: "6478d3436c362a1798dbdd7c",
        nome: "mouse gamer",
        categoria: "games",
        preco: 101,
        descricao: "Mouse com leds.",
    },
    {
        _id: "6478d3436c362a1798dbdd7d",
        nome: "teclado gamer",
        categoria: "games",
        preco: 99,
        descricao: "Teclado com leds.",
    },
    {
        _id: "6478d3436c362a1798dbdd7e",
        nome: "monitor gamer",
        categoria: "games",
        preco: 1500,
        descricao:
        "Monitor grande para jogar.",
    },
    {
        _id: "6478d3436c362a1798dbdd7f",
        nome: "jogo batman",
        categoria: "games",
        preco: 150,
        descricao:
        "Jogo do Batman para PC.",
    },
    {
        _id: "6478d3436c362a1798dbdd80",
        nome: "jogo tomb raider",
        categoria: "games",
        preco: 100,
        descricao:
        "Jogo Tomb Raider para PC.",
    },
    {
        _id: "6478d3436c362a1798dbdd81",
        nome: "jogo spider-man",
        categoria: "games",
        preco: 200,
        descricao:
        "Jogo Spider-man para PS4.",
    },
    {
        _id: "6478d3436c362a1798dbdd82",
        nome: "jogo pac-man",
        categoria: "games",
        preco: 180,
        descricao:
        "Jogo Pac-man para Xbox One.",
    },
    ];

#### 3. Realizar uma consulta que apresente produtos com preço "0";

#### Comando:

    db.collection.find({preco: 0})

#### Retorno:

    [
    {
        _id: "6478d4176c362a1798dbe1e4",
        nome: "adesivo",
        categoria: "utilitários",
        preco: 0,
        descricao:
        "desivo com precificação para produtos.",
    },
    {
        _id: "6478d4176c362a1798dbe1e5",
        nome: "caneca",
        categoria: "utilitários",
        preco: 0,
        descricao: "Caneca para café.",
    },
    ];

#### 4. Realizar uma consulta que apresente produtos com o preço maior que "100.00";

#### Comando:

    db.collection.find({preco: {$gt:100}})

#### Retorno:

    [
    {
        _id: "6478de6a320038884291ce47",
        nome: "mouse gamer",
        categoria: "games",
        preco: 101,
        descricao: "Mouse com leds.",
    },
    {
        _id: "6478de6a320038884291ce49",
        nome: "monitor gamer",
        categoria: "games",
        preco: 1500,
        descricao:
        "Monitor grande para jogar.",
    },
    {
        _id: "6478de6a320038884291ce4a",
        nome: "jogo batman",
        categoria: "games",
        preco: 150,
        descricao:
        "Jogo do Batman para PC.",
    },
    {
        _id: "6478de6a320038884291ce4c",
        nome: "jogo spider-man",
        categoria: "games",
        preco: 200,
        descricao:
        "Jogo Spider-man para PS4.",
    },
    {
        _id: "6478de6a320038884291ce4d",
        nome: "jogo pac-man",
        categoria: "games",
        preco: 180,
        descricao:
        "Jogo Pac-man para Xbox One.",
    },
    {
        _id: "6478de6a320038884291ce4e",
        nome: "guarda-roupas lady bug",
        categoria: "casa",
        preco: 2500,
        descricao:
        "Guarda-roupas gigante da Lady Bug.",
    },
    {
        _id: "6478de6a320038884291ce4f",
        nome: "cama solteiro",
        categoria: "casa",
        preco: 1800,
        descricao: "Cama box solteiro.",
    },
    ];

#### 5. Realizar uma consulta que apresente produtos com o preço entre "1000.00" e "2000.00";

#### Comando:

    db.collection.find({preco: {$gt:1000, $lt:2000}})

#### Retorno:

    [
    {
        _id: "6478dee66c362a1798dc14e6",
        nome: "monitor gamer",
        categoria: "games",
        preco: 1500,
        descricao:
        "Monitor grande para jogar.",
    },
    {
        _id: "6478dee66c362a1798dc14ec",
        nome: "cama solteiro",
        categoria: "casa",
        preco: 1800,
        descricao: "Cama box solteiro.",
    },
    ];

#### 6. Realizar uma consulta que apresente produtos em que o nome contenha a palavra "jogo".

#### Comando:

    db.collection.find({nome: /jogo/}) 

#### Retorno:

    [
    {
        _id: "6478dfc26c362a1798dc17a0",
        nome: "jogo batman",
        categoria: "games",
        preco: 150,
        descricao:
        "Jogo do Batman para PC.",
    },
    {
        _id: "6478dfc26c362a1798dc17a1",
        nome: "jogo tomb raider",
        categoria: "games",
        preco: 100,
        descricao:
        "Jogo Tomb Raider para PC.",
    },
    {
        _id: "6478dfc26c362a1798dc17a2",
        nome: "jogo spider-man",
        categoria: "games",
        preco: 200,
        descricao:
        "Jogo Spider-man para PS4.",
    },
    {
        _id: "6478dfc26c362a1798dc17a3",
        nome: "jogo pac-man",
        categoria: "games",
        preco: 180,
        descricao:
        "Jogo Pac-man para Xbox One.",
    },
    ];

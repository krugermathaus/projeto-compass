# Day 7

## SQL

SQL (Structured Query Language) é um conjunto de comandos de maniplação de banco de dados para criar e manter a estrutura desse banco, além de incluir, excluir, modificar e pesquisar informações nas tabelas dele. É uma linguagem declarativa.

### Subconjuntos da linguagem abordados no curso

- DDL (Data Definition Language)
   
    CREATE TABLE;
    ALTER TABLE;
    DROP TABLE;

- DML (Data Manipulation Language)

    INSERT;
    DELETE;
    UPDATE;

## Atividades realizadas:

### Com base na tabela de Usuários, você deve:

#### 1. Realizar uma consulta que conte o número de registros na tabela.

#### Comando:

**SELECT COUNT (*) FROM usuarios;**

#### Retorno:

| Count |
|---|
|**15**|

#### 2. Realizar uma consulta para encontrar o usuário com o id 10.

#### Comando:

**SELECT * FROM usuarios WHERE id = 10;**

#### Retorno:

| id | nome | email |
|---|---|---|
| 10 | Lucca Ryan Jesus | luccaryanjesus@imoveisvillani.com.br|

#### 3. Realizar uma consulta para encontrar o usuário com o nome "Bruce Wayne".

#### Comando:

**SELECT * FROM usuarios WHERE nome = 'Bruce Wayne';**

#### Retorno:

| id | nome | email |
|---|---|---|
| 13 | Bruce Wayne | brucewayne@gothan.com|

#### 4. Realizar uma consulta para encontrar o usuário com o e-mail "ghost_silva@fantasma.com".

#### Comando:

**SELECT * FROM usuarios WHERE email = 'ghost_silva@fantasma.com';**

#### Retorno:

| id | nome | email |
|---|---|---|
| 4 | Ghost Silva | ghost_silva@fantasma.com|

### 5. Realizar uma consulta para deletar o usuário com e-mail "peterparker@marvel.com".

#### Comando:

**DELETE FROM usuarios WHERE nome = 'Peter Parker';**

#### Retorno:

Não há retorno e, aparentemente a linha também não é deletada do banco de dados. Inicialmente pensei ser algum erro, mas de acordo com o colega **Matheus Casagrande** o SQLiteOnline realmente não deleta a linha. Obrigado Matheus!

Onde estará Peter Parker?

![](https://images.complex.com/complex/images/c_fill,dpr_auto,f_auto,q_auto,w_1400/fl_lossy,pg_1/bdnkabiqxpxmxzmcaluz/spider-man-meme?fimg-ssr)

## Com base na tabela de Produtos, você deve:

### 1. Realizar uma consulta que apresente produtos com descrição vazia;

#### Comando:

**SELECT * FROM produtos WHERE descricao = '';**

#### Retorno:

| id | nome | categoria | preco | descricao|
|---|---|---|---|---|
| 5 | caneca chopp | utilitários | 25.5 | |
| 6 | copo grande térmico | utilitários | 35.9 | |

### 2. Realizar uma consulta que apresente produtos com a categoria "games";

#### Comando:

**SELECT * FROM produtos WHERE categoria = 'games';**

#### Retorno:

| id | nome | categoria | preco | descricao|
|---|---|---|---|---|
| 7 | mouse gamer | games | 101 | Mouse com leds. |
| 8 | teclado gamer | games | 99 | Teclado com leds. |
| 9 | monitor gamer | games | 1500 | Monitor grande para jogar. |
| 10 | jogo batman | games | 150 | Jogo do Batman para PC. |
| 11 | jogo tomb rainder |  games| 100 | Jogo Tomb Raider para PC. |
| 12 | jogo spider-man | games | 200 | Jogo Spider-man para PS4. |
| 13 | jogo pac-man | games | 180 | Jogo Pac-man para Xbox One. |

### 3. Realizar uma consulta que apresente produtos com preço "0";

#### Comando:

**SELECT * FROM produtos WHERE preco = 0;**

#### Retorno:

| id | nome | categoria | preco | descricao|
|---|---|---|---|---|
| 3 | adesivo | utilitários | 0 | Adesivo com precificação para produtos. |
| 4 | caneca | utilitários | 0 | Caneca para café |

### 4. Realizar uma consulta que apresente produtos com o preço maior que "100.00";

#### Comando:

 **SELECT * FROM produtos WHERE preco > 100;**

#### Retorno:

| id | nome | categoria | preco | descricao|
|---|---|---|---|---|
| 7 | mouse gamer | games | 101 | Mouse com leds. |
| 9 | monitor gamer | games | 1500 | Monitor grande para jogar.|

### 5. Realizar uma consulta que apresente produtos com o preço entre "1000.00" e "2000.00";

#### Comando:

**SELECT * FROM produtos WHERE preco > 1000 AND preco < 2000;**

#### Retorno:

| id | nome | categoria | preco | descricao|
|---|---|---|---|---|
| 9 | monitor gamer | games | 1500 | Monitor grande para jogar.|
| 15 | cama solteiro | casa | 1800 | Cama box solteiro. |

### 6. Realizar uma consulta que apresente produtos em que o nome contenha a palavra "jogo";

#### Comando:

**SELECT * FROM produtos WHERE nome LIKE '%jogo%';**

#### Retorno:

| id | nome | categoria | preco | descricao|
|---|---|---|---|---|
| 10 | jogo batman | games | 150 | Jogo do Batman para PC. |
| 11 | jogo tomb raider | games | 100 | Jogo Tomb Raider para PC. |

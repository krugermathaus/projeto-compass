# Day 4

## Fundamentos do teste de software

## Dinâmica: 

### Como um QA pode gerar qualidade no projeto?

### Com base nos Sete Princípios dos Testes, podemos concluir que:

1. Testes mostram a presença defeitos, posibilitando correções e melhorias, tornando o produto mais confiável e entregando uma melhor experiência para o usuário final.

2. É necessário saber quando e quais testes utilizar, testes exaustivos se tornão impossíveis devido ao nível de complexidade dos sistemas.

3. Com os testes corretos, se economiza tempo e dinheiro, evitando retrabalhos e falhas no produto final.

4. Se os testes forem negligenciados ou aplicados de forma incorreta, erros podem passar despercebidos e se acumularem, trazendo consequências maiores.

5. Os testes precisam ser atualizados, pois do contrário podem funcionar como um pesticida, que perde sua eficácia com o tempo.

6. Testes são importantes e essenciais, porém depende do contexto. Qual o prazo de entrega? Quais os requisitos? Dependendo do tempo disponível, os testes deverão ser abordados de maneira diferente.

7. Erros sempre existirão, o papel do QA é tentar identificá-los, através dos testes e, se possível, evitá-los, através da melhoria e paronização dos processos.

### Assuntos abordados na reunião do grupo 4:

- Erro na amazon permite acumular cupons de desconto causando prejuizos a empresa.
"De acordo com os consumidores, a Amazon logo suspendeu a validade dos cupons cumulativos e não permite mais que novos clientes vinculem vouchers para obter até 100% de desconto sobre o valor dos produtos."

Este erro poderia ter sido evitado pela testagem de qualidade que encontraria o erro e o reportaria para a equipe de desenvolvedores.

Alguns dos fundamentos do teste de qualidade que evitariam este erro são: Identificar defeitos, reduzir riscos e confiança do software.

https://olhardigital.com.br/2022/01/26/pro/possivel-bug-na-amazon-faz-consumidores-comprarem-quase-de-graca/


- Além dos casos do Ifood e do foguete Ariane 501 Flight(Erro de ponto flutuante causa prejuizo de meio bilhão de dolares)


https://itsfoss.com/a-floating-point-error-that-caused-a-damage-worth-half-a-billion/

https://tecnoblog.net/noticias/2018/09/17/ifood-falha-cupons/
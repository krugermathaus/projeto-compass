## Day 5

# Fundamentos do teste de software (Back-End)

- Teste unitário: Verifiacam a menor unidade de código testável da aplicação, indepente da interação dela com outras partes do código. Pode ser executada antes de se ter o código completo.

- Teste de integração: Avalia o funcionamento de vários módulos integrados para uma única unidade. Busca encontrar falhas entre várias interfaces do software.

- Teste E2E: Testes de ponta a ponta, ou seja, buscam simular o funcionamento da aplicação real.
    
Ciclo de teste E2E:
- Cadastrar
- Escolher item
- Calcular frete
- Sacola
- Pagamento
- Finalizar pedido
- Informações do pedido
- Boleto
- Meus pedidos
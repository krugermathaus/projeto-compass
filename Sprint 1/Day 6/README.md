# Day 6

# Myers e o Princípio de Pareto

"Testar é analisar um programa com a intenção de descobrir erros e defeitos." (Myers)

## Regra 10
    
Quanto antes os testes puderem ser implementados durante o ciclo de desenvolvimento, menor será o custo final de produção, devido a detecção de falhas antecipadamente, que poderiam se tornar problemas maiores (e mais caros) no futuro.

## Princípio de Pareto   
    
80% do resultado, das consequências, são causados por apenas 20% das causas. Ou seja, é possível priorizar e focar em apenas 20% de um processo, obtendo de maneira otimizado um impacto em 80% do resultado.

## Casos onde testes foram negligenciados

- Magazine Luiza

Foi gerado um cupom de R$ 1 mil reais. Se os testes tivessem sido bem planejados e executados, este problema poderia ter sido evitado.

- Sony

Falha de segurança permitiu que Hacker acessasse dados de 77 milhões de usuários de Playstation. Estas falhas de segurança podem ser evitadas com testes.


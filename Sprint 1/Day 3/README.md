# Day 3 | Scrum, Papéis e Responsabilidades

## Desenvolvedores
Equipe que trabalha para o desenvolvimento do produto ou projeto. Interessante que seja autogerenciável. 
    
Responsabilidades: 
- Estimar os itens do Backlog do produto
- Realizar atividades para gerar um incremento do produto
- Gerencias/Atualizar o Backlog da Sprint
- Criar uma definição de "pronto" e acordar com o Product Owner

## Product Owner
Representa os clientes e usuários do sistema/produto.

Responsabilidades:
- Estabelecer a visão e maximizar o ROI do produto
- Garantir que o Backlog do produto seja transparente, visível e compreensível
- Fornecer feedback na revisão da sprint
- Fazer o planejamento das releases
- Decidir quando liberar um incremento em produção

## Scrum Master
É um líder que atua como facilitador do "time" e da organização.

Responsabilidades: 
- Eficácia do time, permitindo que ele melhore suas práticas dentro do framework.
- Atender ao time: treinando, ajudando a focar na criação de incremento de alto valor
- Atender ao PO, ex: Ajudando a encontrar técnicas para definição de metas o produto
- Atender a organização, ex: planejar e aconselhar implemetações de scrum dentro da organização

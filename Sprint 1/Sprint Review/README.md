# Síntese

## Qualidade, testes, cybersegurança e defesa pessoal.

### Defesa pessoal: Conflito, pré-conflito, prevenção.
![](https://media.istockphoto.com/id/150379293/pt/vetorial/f%C3%AAmea-auto-defesa.jpg?s=612x612&w=0&k=20&c=MvQw1jo9bYoj45E_rj4uobg5txk0XjKwweYY-v6i6U8=)

### Ciclo OODA. Antever, prevenir.
![](https://donadelas.com.br/system/media_attachments/files/106/184/789/633/157/276/original/3f3e0df2e84cbb7f.jpg)

### Por que a galinha atravessou a rua?
![](https://i0.wp.com/blog.lucianoreis.com/wp-content/uploads/2020/08/Galinha-Atravessando-a-Rua.jpg?w=512&ssl=1)

### Comportamento humano, erros, possibilidades, psicologia, métodos ágeis **PESSOAS**.

![](https://backefront.com.br/posts/Figura1_ErroFalhaDefeito.jpg)
 

### Testes? Qualidade? Vulnerabilidades, comportamentos irracionais, má intenção.

![](https://testvox.com/wp-content/uploads/2023/02/why-QA-is-important-768x768.jpg)

### Pessoas.
![](https://www.lotus-qa.com/wp-content/uploads/2020/04/Psychology-of-Technology-Conference.jpg)
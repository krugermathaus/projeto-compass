# Scrum

Scrum é um framework simples para o gerenciamento de projetos complexos.

## Pilares Fundamentias

- Transparência: Todos tem conhecimento dos requisitos dos processos e do andamento do projeto.
- Inspeção: Tudo que acontece e inspecionado, como através das Dailies ou do Sprint Review.
- Adaptação: O produto sofre adaptações através das mudanças implementadas, e o processo também pode ser adaptado à realidade da empresa.

## Práticas Fundamentais

### Papéis

- Scrum Master: Funciona como um coach, um facilitador, orientando e incentivando a equipe.
- Product Owner: É o líder máximo do projeto, que toma as decisões principais. 
- Dev Team: São as pessoas que de fato constroem o projeto.

### Eventos

- Sprint Planning: Reunião de planejamento das Sprints, onde será criado o Backlog 
- Dily Scrum: Reuniões de 15 minutos onde devem ser respondidas as perguntas "O que eu fiz ontem? O que farei hoje? Existe algum impedimento?".

### Artefatos

- Product Backlog: Conjunto de todas as funcionalidades desejadas, que serão desenvolvidas de acordo com a organização definida pelo PO.
- Sprint Review: Verifica em retrospectiva necessidades de adaptação do produto e do processo.

## Burndown Chart

Gráfico que relaciona os items a fazer com o tempo necessário.

## Kanban Board

Sistema de visualização do fluxo de desenvolvimento de um projeto, podendo ser divido em Backlog, To do, Doing e Done, por exemplo. Podem ser utilizadas ferramentas digitais como Trello, ou ser feito em uma lousa e/ou com postits também.
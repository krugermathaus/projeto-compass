# Sprint 1 | Day 1

Neste primeiro dia será abordado o Git e GitLab.

## Git e GitLab

Git é um sistema de controle de versões de software, enquanto o GitLab é uma ferramenta de gerenciamento de repositóios baseado em Git.

## Comandos básicos em Git

- git init: Inicia um novo projeto que ainda não é um repositório.
- git status: Exibe as condições do diretório de trabalho e da área de staging.
- git add: Inclui as alterações de um arquivo no próximo commit.
- git commit: Define um ponto de verificação no processo de desenvolvimento, ou seja, um commit.

# Day 9

# Segurança da informação

## Como ser hackeado?

Palestra muito bom, bem humorada, em tom irônico, apontando nossas possíveis falhas comportamentais que nos levam a sermos hackeados tão facilmente. Ou seja, será que antes de pensarmos em Segurança Digital, deveríamos pensar nas nossas "brechas" psicológicas que fazem com que tenhamos comportamentos irracionais?

### Dica 1

Seja vítima de phishing: Clique em todos os links que você ver.

### Dica 2
Compatilhe sua Wi-fi. 

### Dica 3
Mantenha suas completas, atualizadas e públicas.

### Dica 4 
Use a mesma senha para tudo.

### Dica 5
Se você perceber coisas estranhas no seu PC de trabalho, acessos que você não reconhece, por exemplo, nunca avise a segurança.

### Dica 6
Não faça backup, não seja pessimista!

---
## Segurança Digital ou Pessoal?

* "Os alvos são as pessoas e não apenas os Sistemas."

*  "Dos 91 mil incidentes de segurança registrados na *Network Barometer Report*, 85% se relacionam com questões como erro humano."

---

## Novo OWASP Top 10 - 2021
### OWASP é uma organização que tem com objetivo aprimorar a segurança de software. Um dos trabalhos realizados pela organização é o Top 10 de maiores vulnerabilidades em uma aplicação web e é realizado a cada 3 anos, aproximadamente.

## Segurança na rede Wi-fi doméstica
### Ameaças:
* Sequestro de DNS
* Botnets e Proxys
* Monitoramento de Tráfego
* Vazamento de dados pessoais e senhas

### Como se precaver e defender-se destas ameaças?
* Alterar informações padrão do roteador.
* Utilizar WPA2 e AES256.
* Alterar periodicamente nome da rede e senha.
* Ativar o firewall do roteador pessoal.
* Bloquear dispositivos desconhecidos.
* Ocultar o nome da rede.
* Atualizar o firmware dos equipamentos.

### Home-office
* Evitar acessar o ambiente de trabalho em redes públicas.
* Sempre verificar se o antivírus está ativo e atualizado.
Sempre atualizar o sistema com os patchs disponíveis pela empresa.
* Alterar as senhas periodicamente.
* Nunca compartilhar usuários/senhas.

A9: Improper Assets Management: Inventário de APIs incompleto e/ou desatualizado = lacunas.

### Dicas para o dia a dia
* Boas senhas: longas, porém memorizáveis.
* Usar um gerenciador de senhas.
* MFA - Autenticação Multifator.
* Cuidados com phishing (Email é um canal hostil).
* Antivírus.
* Manter software atualizado.
* Backup.
* Segurança no navegador.
* Privacidade.
* Transações na internet.

"O atacante sempre vai onde está mais fácil"





